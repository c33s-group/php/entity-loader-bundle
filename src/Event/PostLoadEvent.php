<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\Event;

use C33s\Bundle\EntityLoaderBundle\Entity\LoadedContentInformation;
use C33s\Bundle\EntityLoaderBundle\EntityLoader\ContentInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class PostLoadEvent extends Event
{
    private array $contents;
    private array $loadedContentInformations;
    private array $skippedContents;
    private array $loadedEntities;

    /**
     * @param ContentInterface[]         $contents
     * @param LoadedContentInformation[] $loadedContentInformations
     * @param ContentInterface[]         $skippedContents
     * @param object[]                   $loadedEntities
     */
    public function __construct(
        array $contents,
        array $loadedContentInformations,
        array $skippedContents,
        array $loadedEntities
    ) {
        $this->contents = $contents;
        $this->loadedContentInformations = $loadedContentInformations;
        $this->skippedContents = $skippedContents;
        $this->loadedEntities = $loadedEntities;
    }

    /**
     * @return ContentInterface[]
     */
    public function getContents(): array
    {
        return $this->contents;
    }

    /**
     * @return ContentInterface[]
     */
    public function getLoadedContentInformations(): array
    {
        return $this->loadedContentInformations;
    }

    /**
     * @return ContentInterface[]
     */
    public function getSkippedContents(): array
    {
        return $this->skippedContents;
    }

    /**
     * @return ContentInterface[]
     */
    public function getLoadedEntities(): array
    {
        return $this->loadedEntities;
    }

    public function getContentsCount(): int
    {
        return count($this->contents);
    }

    public function getLoadedContentsCount(): int
    {
        return count($this->loadedContentInformations);
    }

    public function getSkippedContentsCount(): int
    {
        return count($this->skippedContents);
    }

    public function getLoadedEntitiesCount(): int
    {
        return count($this->loadedEntities);
    }
}
