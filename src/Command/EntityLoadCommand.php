<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\Command;

use C33s\Bundle\EntityLoaderBundle\EntityLoader\ContentLoader;
use DateTimeImmutable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EntityLoadCommand extends Command
{
    protected static $defaultName = 'content:load';
    protected static $defaultDescription = '';
    private ContentLoader $contentLoader;

    public function __construct(ContentLoader $contentLoader, $name = null)
    {
        $this->contentLoader = $contentLoader;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('prevent-load-before-date', null, InputOption::VALUE_REQUIRED, '')
            ->addOption('prevent-load-after-date', null, InputOption::VALUE_REQUIRED, '')
            ->addOption('current-date', null, InputOption::VALUE_REQUIRED, '')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $preventLoadBeforeDate = $input->getOption('prevent-load-before-date');
        $preventLoadAfterDate = $input->getOption('prevent-load-after-date');
        $currentDate = $input->getOption('current-date');

        if (null !== $preventLoadBeforeDate && is_string($preventLoadBeforeDate)) {
            $date = new DateTimeImmutable($preventLoadBeforeDate);
            $this->contentLoader->setGlobalPreventLoadBeforeDate($date);
        }
        if (null !== $preventLoadAfterDate && is_string($preventLoadAfterDate)) {
            $date = new DateTimeImmutable($preventLoadAfterDate);
            $this->contentLoader->setGlobalPreventLoadAfterDate($date);
        }
        if (null !== $currentDate && is_string($currentDate)) {
            $date = new DateTimeImmutable($currentDate);
            $this->contentLoader->setCurrentDate($date);
        }
        $loadedEntities = $this->contentLoader->load();
        $loadedEntitiesCount = count($loadedEntities);
        $io->success("Loaded $loadedEntitiesCount parent-entities.");

        return 0;
    }
}
