<?php

declare(strict_types=1);

namespace C33s\Bundle\EntityLoaderBundle\EntityLoader;

use C33s\Bundle\EntityLoaderBundle\Entity\LoadedContentInformation;
use C33s\Bundle\EntityLoaderBundle\Event\PostLoadEvent;
use C33s\Bundle\EntityLoaderBundle\Event\PreLoadEvent;
use C33s\Bundle\EntityLoaderBundle\Repository\LoadedContentInformationRepository;
use Carbon\CarbonImmutable;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Traversable;

final class ContentLoader
{
    /**
     * @var ContentInterface[]
     */
    private array $contents;
    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;
    private LoadedContentInformationRepository $loadedContentRepository;
    private ?CarbonImmutable $globalPreventLoadBeforeDate = null;
    private ?CarbonImmutable $globalPreventLoadAfterDate = null;
    private ?CarbonImmutable $currentDate = null;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        Traversable $contentFilesToLoad,
        EntityManagerInterface $entityManager,
        LoadedContentInformationRepository $loadedContentsRepository,
        ?LoggerInterface $logger = null
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->contents = iterator_to_array($contentFilesToLoad);
        $this->entityManager = $entityManager;
        $this->loadedContentRepository = $loadedContentsRepository;
        $this->logger = $logger ?? new NullLogger();
    }

    public function setGlobalPreventLoadBeforeDate(DateTimeInterface $globalPreventLoadBeforeDate): void
    {
        $this->globalPreventLoadBeforeDate = new CarbonImmutable($globalPreventLoadBeforeDate);
    }

    public function setGlobalPreventLoadAfterDate(DateTimeInterface $globalPreventLoadAfterDate): void
    {
        $this->globalPreventLoadAfterDate = new CarbonImmutable($globalPreventLoadAfterDate);
    }

    public function setCurrentDate(DateTimeInterface $currentDate): void
    {
        $this->currentDate = new CarbonImmutable($currentDate);
    }

    /**
     * @return object[]
     */
    public function load(): array
    {
        $this->eventDispatcher->dispatch(new PreLoadEvent($this->contents));
        $loadedContentInformations = [];
        $skippedContents = [];
        $loadedEntities = [];
        /** @var ContentInterface $content */
        foreach ($this->contents as $content) {
            if (!$this->shouldLoad($content)) {
                $skippedContents[] = $content;
                continue;
            }
            $entities = $content->getEntities();
            $entityCount = count($entities);
            $contentClass = get_class($content);
            $this->logger->info("Adding $entityCount parent-entities from $contentClass");
            if ($content->shouldFlushBefore()) {
                $this->entityManager->flush();
            }
            foreach ($entities as $entity) {
                $class = get_class($entity);
                $this->logger->debug("Adding $entityCount parent-entities from content class '$class'");
                $this->entityManager->persist($entity);
                $loadedEntities[] = $entity;
            }
            if ($content->shouldFlushAfter()) {
                $this->entityManager->flush();
            }
            //store info
            $loadedContentInformations[] = $this->saveLoadedContentInformation($content, $entityCount);
        }
        $this->entityManager->flush();
        $this->eventDispatcher->dispatch(new PostLoadEvent(
            $this->contents,
            $loadedContentInformations,
            $skippedContents,
            $loadedEntities
        ));

        return $loadedEntities;
    }

    private function saveLoadedContentInformation(ContentInterface $content, int $entityCount): LoadedContentInformation
    {
        $loadedContent = LoadedContentInformation::createFromClass($content, $entityCount);
        $this->entityManager->persist($loadedContent);

        return $loadedContent;
    }

    private function shouldLoad(ContentInterface $contents): bool
    {
        $contentClass = get_class($contents);
        $now = $this->currentDate ?? new CarbonImmutable();

        if (null !== $this->globalPreventLoadAfterDate && $now->isAfter($this->globalPreventLoadAfterDate)) {
            $date = $this->globalPreventLoadAfterDate->format('Y-m-d');
            $this->logger->info("Skipped: Global load date already passed ($date, $contentClass).");

            return false;
        }
        if (null !== $this->globalPreventLoadBeforeDate && $now->isBefore($this->globalPreventLoadBeforeDate)) {
            $date = $this->globalPreventLoadBeforeDate->format('Y-m-d');
            $this->logger->info("Skipped: Global Load date not reached yet ($date, $contentClass).");

            return false;
        }
        if ($now->isAfter($contents->preventLoadAfterDate())) {
            $date = $contents->preventLoadAfterDate()->format('Y-m-d');
            $this->logger->info("Skipped: Load date already passed ($date, $contentClass).");

            return false;
        }
        if ($now->isBefore($contents->preventLoadBeforeDate())) {
            $date = $contents->preventLoadBeforeDate()->format('Y-m-d');
            $this->logger->info("Skipped: Load date not reached yet ($date, $contentClass).");

            return false;
        }
        if ($this->loadedContentRepository->has($contents)) {
            $this->logger->info("Skipped: Content file already loaded ($contentClass).");

            return false;
        }

        return true;
    }
}
